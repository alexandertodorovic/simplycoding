# coding: utf-8
import sklearn.datasets

# Classify from doctors medical estimate/opinion if someone is
# (1) able to work (2) unfit to work (3) incapable of working 
category_map = {'af': 'arbeitsfaehig', 'au': 'arbeitsunfaehig', 'bu': 'berufsunfaehig'}
training_data = sklearn.datasets.load_files(container_path='/home/dslinuxalex/textclassification/python/re',
                                            description='RE', shuffle=True, load_content=True,
                                            random_state=4, encoding='UTF-8')
#dir(training_data)

print('size ', training_data.filenames.size)
print('target ' , training_data.target)
print('target names ', training_data.target_names)
print('description ' , training_data.DESCR)
#print('file names', training_data.filenames)
#print('description ' , training_data.data)


# Feature extraction
from sklearn.feature_extraction.text import CountVectorizer

vectorizer = CountVectorizer()
X_train_termcounts = vectorizer.fit_transform(training_data.data)
print ("\nDimensions of training data:", X_train_termcounts.shape)

# Training a classifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import TfidfTransformer

input_data = [
    "Der Versicherte ist als selbständiger Versicherungsmakler ab dem 15.02.2016 teilweise arbeitsfähig.", 
    "Im Rahmen der Restfunktionseinschränkung, wie oben befundet, kann jetzt noch eine AU in Übereinstimmung mit dem behandelnden Hausarzt bis einschließlich Freitag, den 12.02.2016 bescheinigt werden, aber eine Berufsunfähigkeit liegt nicht vor.",
    "Es ist nicht davon auszugehen, dass Herr K. seine bisher berufliche Tätigkeit in absehbarer Zeit zu mehr als 50 Prozent wieder verrichten kann."
]

# tf-idf transformer
tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_termcounts)

# Multinomial Naive Bayes classifier
classifier = MultinomialNB().fit(X_train_tfidf, training_data.target)

X_input_termcounts = vectorizer.transform(input_data)
X_input_tfidf = tfidf_transformer.transform(X_input_termcounts)


# Predict the output categories
predicted_categories = classifier.predict(X_input_tfidf)

# Print the outputs
for sentence, category in zip(input_data, predicted_categories):
    print ('\nInput:', sentence, '\nPredicted category:',             category_map[training_data.target_names[category]])

#classifier.score()